$(document).ready(function() {

  $.getJSON('data/ingredients.json', function(data) {
    $.each(data, function(ingredient, obj) {
      var ingredientsTag =
      '<div class="col-md-3 col-sm-6">'+
        '<div class="thumbnail">'+
          '<img src="http://placehold.it/800x500" alt="">'+
          '<div class="caption span4 collapse-group">'+
            '<h4>' + ingredient + ' <span class="label pull-right">' + obj.category + '</span></h4>'+
            '<p>' + obj.description + '</p>'+
            // '<p class="collapse">' + obj.description + '</p>'+
            '<p><a href="#" class="btn btn-default transparent" role="button">&raquo; Mehr erfahren</a></p>'+
          '</div>'+
        '</div>'+
      '</div>';

      // Append only the ingredients of menuitem1
      if (obj.menu == "menuitem1") {
        $(".displays").append(ingredientsTag);
      }

      // Add class to fade in/out ingredients and to set the color of category labels
      if (obj.category == "Fleisch") {
        $(".col-md-3").last().addClass("display-meat");
        $("span").last().addClass("label-danger");
      } else if (obj.category == "Gemüse") {
        $(".col-md-3").last().addClass("display-vegetables");
        $("span").last().addClass("label-success");
      } else {
        $(".col-md-3").last().addClass("display-fruit");
        $("span").last().addClass("label-warning");
      }

    });
  });

});
