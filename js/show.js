$(document).ready(function() {

  // Show menus
  $(function () {
     var menus = ["menuitem1", "menuitem2", "menuitem3", "menuitem4", "menuitem5", "menuitem6"];

     $.each(menus, function (index, menu) {
       var menuTag = '<a href="#" class="btn btn-default transparent"><strong>' + menu + '</strong></a>';
       $("header .btn-group").append(menuTag);
     });
  });

  // Show ingredients
  $('.displays').show();

  // Fade in/out ingredients
  $('#meat').on('click', function() {
    $('.display-meat').fadeToggle();
  });
  $('#vegetables').on('click', function() {
    $('.display-vegetables').fadeToggle();
  });
  $('#fruit').on('click', function() {
    $('.display-fruit').fadeToggle();
  });

  // More info
  $('.caption .btn').on('click', function() {
    $('.collapse').slideToggle('slow');
  });

});
